/**
 *  NES Controller receiver code.
 *
 *
 */

#include <wixel.h>
#include <usb.h>
#include <usb_com.h>
#include <radio_link.h>
#include <random.h>
#include <stdio.h>
// Main
void main()
{
	uint8 i;
	uint8 XDATA * packet;
	
	// Do some system initialization. This is wonderful Wixel stuff.
	systemInit();
	usbInit();
	radioLinkInit();

	// Main Loop
	while(1)
	{
		boardService();
		packet = radioLinkRxCurrentPacket();
		if(packet != 0)
		{
			for(i = 0; i < 8; i++)
			{
				usbComTxSendByte(packet[i + 1] + 0x30);
			}
			usbComTxSendByte(0x0A);
			usbComTxSendByte(0x0D);
			radioLinkRxDoneWithPacket();
		}
		usbComService();
	}
}
