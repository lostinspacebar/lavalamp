/**
 *  NES Controller transmitter code.
 *
 *
 */

#include <wixel.h>
#include <usb.h>
#include <usb_com.h>
#include <radio_link.h>
#include <stdio.h>

// Pins we will be monitoring
#define PIN_COUNT 8
static uint8 CODE pins[PIN_COUNT] = {10, 11, 12, 13, 14, 15, 16, 17};
static uint8 lastPacket[PIN_COUNT] = {1, 1, 1, 1, 1, 1, 1, 1};

// Timer used to limit refresh rate
uint32 loopTimer = 0;

// Update button states and output over serial
void updateButtons()
{
	uint8 i;
	uint8 sendPacket = 0;
	
	usbShowStatusWithGreenLed();
	LED_YELLOW(0);

	// Update state
	if(getMs() - loopTimer >= 15)
	{
		uint8 XDATA * packet = radioLinkTxCurrentPacket();
		
		// Read in pins
		packet[0] = 8;
		for(i = 0; i < PIN_COUNT; i++)
		{
			packet[i + 1] = isPinHigh(pins[i]) == HIGH ? 1 : 0;
			
			// Check to see if any state has changed
			if(lastPacket[i] != packet[i + 1])
			{
				// Yup need to send packet
				sendPacket = 1;
			}
			lastPacket[i] = packet[i + 1];
		}
		
		// Send packet if something changed.
		if(sendPacket == 1)
		{
			radioLinkTxSendPacket(0);
			sendPacket = 0;
		}
		
		// Next iteration
		LED_RED(!LED_RED_STATE);
		loopTimer = getMs();
	}
}

// Main
void main()
{
	uint8 i;
	
	// Do some system initialization. This is wonderful Wixel stuff.
	systemInit();
	usbInit();
	radioLinkInit();

	// Initialize GPIO
	for(i = 0; i < PIN_COUNT; i++)
	{
		setDigitalInput(pins[i], HIGH_IMPEDANCE);
	}

	// Main Loop
	while(1)
	{
		boardService();
		updateButtons();
		usbComService();
	}
}
